# mathgame-fe

[1]: https://github.com/yasinaydinnet/mathgame-be

This is the frontend of [mathgame-be][1] project. 

## Project Info

Technologies used:
- Vue 2.x
- Vue-cli 4.x
- Vuex
- Websockets
- Linting with ESLint + Prettier
- Bootstrap 4.x

About project:
- Linting as pre-commit hook
- No tests

## Development

Note: Instructions here are provided for Linux/Unix/Mac.

Requirements:
- Git
- Node.js
- Yarn (you can also use NPM, but I personally prefer Yarn)

To develop and run the project locally, do the following steps in a shell. By default, it will try to use local websocket server. See [mathgame-be][1] project on how to set it up.

```sh
git clone git@github.com:yasinaydinnet/mathgame-fe.git
cd mathgame-fe
yarn
yarn serve
```

The frontend is accessible at http://localhost:8080/

You can also use Websocket server deployed on Heroku (see `.env.production` file) or use your own server by creating a `.env.local` file (it is not pushed to git). 

## Deploying to Production

This app is already configured to be auto-deployed to a Heroku free-tier machine and is residing at https://mathgame-fe.herokuapp.com/

To deploy somewhere else, you have to run `yarn build`, which will transpile the project and create a `dist/` folder for output. You need to serve these files, as for such purpose you can use `yarn start`. 
