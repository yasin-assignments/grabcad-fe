var StaticServer = require("static-server");

const PORT = process.env.PORT || 8777;

const server = new StaticServer({
  rootPath: "./dist",
  port: PORT,
  cors: "*"
});

server.start();
