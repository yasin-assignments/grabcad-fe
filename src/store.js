import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    socket: {
      isConnected: false,
      serverFull: false,
      scores: []
    }
  },

  mutations: {
    SOCKET_ONOPEN(state) {
      state.socket.serverFull = false;
    },

    SOCKET_ONCLOSE(state, event) {
      state.socket.isConnected = false;

      if (event.code == 4001) {
        state.socket.serverFull = true;
      } else {
        state.socket.serverFull = false;
      }

      if (state.socket.serverFull) {
        setTimeout(Vue.prototype.$connect, 5000);
      } else {
        setTimeout(Vue.prototype.$connect, 1000);
      }
    },

    SOCKET_ONMESSAGE(state, message) {
      state.socket.isConnected = true;

      const cp = { ...state.socket };

      for (let msg in message) {
        if (msg == "scores") {
          // Convert scores from {} to []
          const arr = [];
          Object.keys(message[msg]).forEach(id => {
            arr.push({ id, score: message[msg][id] });
          });
          cp[msg] = arr;
        } else {
          cp[msg] = message[msg];
        }
      }
      state.socket = cp;
    }
  }
});
