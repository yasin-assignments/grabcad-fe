import Vue from "vue";
import App from "./App.vue";
import VueNativeWebSocket from "vue-native-websocket";
import store from "./store.js";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

const URL = process.env.VUE_APP_WSSERVER;
Vue.use(VueNativeWebSocket, URL, {
  format: "json",
  store: store,
  connectManually: true
});

Vue.config.productionTip = false;

const vm = new Vue({
  store,
  render: h => h(App)
}).$mount("#app");

vm.$connect();
